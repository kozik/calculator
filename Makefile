IDIR=inc
SDIR=src
CDIR=CMSIS
GNUDIR=/home/rk/gcc-arm-none-eabi-7-2018-q2-update/bin

CC=$(GNUDIR)/arm-none-eabi-gcc
CFLAGS=-I$(IDIR) -I$(CDIR) -mcpu=cortex-m0 -mthumb -g -Wall -O0

ODIR=obj

_DEPS=calc.h
_DEPS+=defs.h
_DEPS+=ui.h
DEPS=$(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ=main.o ui.o calc.o
OBJ=$(patsubst %,$(ODIR)/%,$(_OBJ))

main.elf: startup.o $(OBJ)
	$(CC) $(CFLAGS) -T linker.ld -nostartfiles -o $@ $^ 
	$(GNUDIR)/arm-none-eabi-objcopy -S -O binary main.elf main.bin
	
$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

startup.o: startup.S
	$(CC) -c -o $@ startup.S $(CFLAGS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ tinytest.o tinytest
