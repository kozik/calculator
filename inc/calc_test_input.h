/*
 *  Data for input test
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#ifndef CALC_TEST_INPUT_H
#define CALC_TEST_INPUT_H
#include "calc.h"

struct simulation_step test_input0[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_1
    }, {
        .state = {
            .memory = 0l,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_8
    }, {
        .state = {
            .memory = 0l,
            .display = 18l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_DOT
    }, {
        .state = {
            .memory = 0l,
            .display = 18l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 0,
            .digits = 2
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 0l,
            .display = 182l * FP_MULTIPLAYER / 10,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 1,
            .digits = 3
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_input1[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_DOT,
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 0,
            .digits = 1
        },
        .b = BUTTON_0,
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 1,
            .digits = 2
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 2,
            .digits = 3
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 3,
            .digits = 4
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 4,
            .digits = 5
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 5,
            .digits = 6
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 5,
            .digits = 6
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 5,
            .digits = 6
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_input2[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_SUB
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_1
    }, {
        .state = {
            .memory = 0l,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_PI
    },  {
        .state = {
            .memory = 0l,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_NONE
    },
};

struct simulation_step test_input3[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_SUB
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_DOT
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 0,
            .digits = 2
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 1,
            .digits = 3
        },
        .b = BUTTON_1
    }, {
        .state = {
            .memory = 0l,
            .display = 0.01l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 2,
            .digits = 4
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_input4[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_PI
    }, {
        .state = {
            .memory = 0l,
            .display = FP_PI,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_PI_DOT,
            .digits = FP_PI_DIGIT
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 0l,
            .display = FP_PI,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_PI_DOT,
            .digits = FP_PI_DIGIT
        },
        .b = BUTTON_NONE
    },
};

struct simulation_step test_input5[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = 0,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_SUB
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_4
    } ,{
        .state = {
            .memory = 0l,
            .display = 4l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_DOT
    }, {
        .state = {
            .memory = 0l,
            .display = 4l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 0,
            .digits = 2
            },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 0l,
            .display = 42l * FP_MULTIPLAYER / 10,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 1,
            .digits = 3},
        .b = BUTTON_1
    }, {
        .state = {
            .memory = 0l,
            .display = 421l * FP_MULTIPLAYER / 100,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 2,
            .digits = 4
        },
        .b = BUTTON_DOT
    }, {
        .state = {
            .memory = 0l,
            .display = 421l * FP_MULTIPLAYER / 100,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 2,
            .digits = 4
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 4210l * FP_MULTIPLAYER / 1000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 3,
            .digits = 5
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 0l,
            .display = 42102l * FP_MULTIPLAYER / 10000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 4,
            .digits = 6
        },
        .b = BUTTON_3
    }, {
        .state = {
            .memory = 0l,
            .display = 42102l * FP_MULTIPLAYER / 10000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 4,
            .digits = 6
        },
        .b = BUTTON_3
    }, {
        .state = {
            .memory = 0l,
            .display = 42102l * FP_MULTIPLAYER / 10000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 4,
            .digits = 6
        },
        .b = BUTTON_PI
    }, {
        .state = {
            .memory = 0l,
            .display = 42102l * FP_MULTIPLAYER / 10000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 4,
            .digits = 6
        },
        .b = BUTTON_4,
        }, {
        .state = {
            .memory = 0l,
            .display = 42102l * FP_MULTIPLAYER / 10000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 4,
            .digits = 6
        },
        .b = BUTTON_DOT,
    }, {
        .state = {
            .memory = 0l,
            .display = 42102l * FP_MULTIPLAYER / 10000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 4,
            .digits = 6
        },
        .b = BUTTON_8
    }, {
        .state = {
            .memory = 0l,
            .display = 42102l * FP_MULTIPLAYER / 10000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 4,
            .digits = 6},
        .b = BUTTON_PI
    }, {
        .state = {
            .memory = 0l,
            .display = 42102l * FP_MULTIPLAYER / 10000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 4,
            .digits = 6
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_input6[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_1
    }, {
        .state = {
            .memory = 0l,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_NONE
    }
};


struct simulation_step test_input7[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = 0,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_DOT
    }, {
        .state = {
            .memory = 0l,
            .display = 0l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 0,
            .digits = 1
        },
        .b = BUTTON_0
    }, {
        .state = {
            .memory = 0l,
            .display = 0l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 1,
            .digits = 2
        },
        .b = BUTTON_1
    }, {
        .state = {
            .memory = 0l,
            .display = 1l * FP_MULTIPLAYER / 100,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 2,
            .digits = 3
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 0l,
            .display = 12l * FP_MULTIPLAYER / 1000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 3,
            .digits = 4
        },
        .b = BUTTON_DOT
    }, {
        .state = {
            .memory = 0l,
            .display = 12l * FP_MULTIPLAYER / 1000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 3,
            .digits = 4
        },
        .b = BUTTON_3
    }, {
        .state = {
            .memory = 0l,
            .display = 123l * FP_MULTIPLAYER / 10000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 4,
            .digits = 5
        },
        .b = BUTTON_4
    }, {
        .state = {
            .memory = 0l,
            .display = 1234l * FP_MULTIPLAYER / 100000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 5,
            .digits = 6
        },
        .b = BUTTON_5
    }, {
        .state = {
            .memory = 0l,
            .display = 1234l * FP_MULTIPLAYER / 100000,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 5,
            .digits = 6
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_input8[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 0l,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_5
    }, {
        .state = {
            .memory = 0l,
            .display = 25l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_DOT
    }, {
        .state = {
            .memory = 0l,
            .display = 25l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 0,
            .digits = 2
        },
        .b = BUTTON_NONE
    },
};

struct simulation_step *test_input_s[] =
    {test_input0, test_input1, test_input2, test_input3, test_input4,
     test_input5, test_input6, test_input7, test_input8, NULL};

#endif /* CALC_TEST_INPUT_H */