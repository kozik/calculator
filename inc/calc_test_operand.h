/*
 *  Data for operand test
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#ifndef CALC_TEST_OPERAND_H
#define CALC_TEST_OPERAND_H
#include "calc.h"

struct simulation_step test_operand0[] = {
    {
        .state = 
        {
            .memory = 0l,
            .display = 12l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_ADD
    }, {
        .state = {
            .memory = 12l * FP_MULTIPLAYER,
            .display = 12l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_ADD,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};


struct simulation_step test_operand1[] = {
    {
        .state = {
            .memory = 0l,
            .display = 871l * FP_MULTIPLAYER / 10,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 1,
            .digits = 3
        },
        .b = BUTTON_SUB
    }, {
        .state = {
            .memory = 871l * FP_MULTIPLAYER / 10,
            .display = 871l * FP_MULTIPLAYER / 10,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_SUB,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand2[] = {
    {
        .state = {
            .memory = 2l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_EQ
    }, {
        .state = {
            .memory = 2l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_1
    }, {
        .state = {
            .memory = 2l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand3[] = {
    {
        .state = {
            .memory = 21l * FP_MULTIPLAYER / 10,
            .display = 3l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_MUL,
            .sign = FP_NO_SIGN,
            .dot_position = 1,
            .digits = 2
        },
        .b = BUTTON_ADD
    }, {
        .state = {
            .memory = 63l * FP_MULTIPLAYER / 10,
            .display = 63l * FP_MULTIPLAYER / 10,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_ADD,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand4[] = {
    {
        .state ={
            .memory = 44l * FP_MULTIPLAYER / 10,
            .display = 22l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_DIV,
            .sign = FP_NO_SIGN,
            .dot_position = 1,
            .digits = 2
        },
        .b = BUTTON_MUL
    }, {
        .state = {
            .memory = 2l * FP_MULTIPLAYER / 10,
            .display = 2l * FP_MULTIPLAYER / 10,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_MUL,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand5[] = {
    {
        .state = {
            .memory = 1l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_DIV,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_ADD
    }, {
        .state = {
            .memory = -5l * FP_MULTIPLAYER / 10,
            .display = 5l * FP_MULTIPLAYER / 10,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_ADD,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand6[] = {
    {
        .state = {
            .memory = 1l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_ADD,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_MUL
    }, {
        .state = {
            .memory = -1l * FP_MULTIPLAYER,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_MUL,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand7[] = {
    {
        .state = {
            .memory = 999999l * FP_MULTIPLAYER,
            .display = 999999l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_MUL,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 6
        },
        .b = BUTTON_MUL
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_OVER,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand8[] = {
    {
        .state = {
            .memory = 999999l * FP_MULTIPLAYER,
            .display = 0l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_DIV,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 6
        },
        .b = BUTTON_MUL
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_DIV_0,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand9[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_SQRT
    }, {
        .state = {
            .memory = 0l,
            .display = 0l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand10[] = {
    {
        .state = {
            .memory = 0l,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_SQRT
    }, {
        .state = {
            .memory = 1l * FP_MULTIPLAYER,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand11[] = {
    {
        .state = {
            .memory = 0l,
            .display = 121l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 3
        },
        .b = BUTTON_SQRT
    }, {
        .state = {
            .memory = 11l * FP_MULTIPLAYER,
            .display = 11l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand12[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_SQRT
    }, {
        .state = {
            .memory = 0l * FP_MULTIPLAYER,
            .display = 0l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operand13[] = {
    {
        .state = {
            .memory = 0l,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_SQRT
    }, {
        .state = {
            .memory = 0l * FP_MULTIPLAYER,
            .display = 0l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_SQRT_NEG,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step *test_operand_s[] =
    {test_operand0, test_operand1, test_operand2, test_operand3, test_operand4,
     test_operand5, test_operand6, test_operand7, test_operand8, test_operand9,
     test_operand10, test_operand11, test_operand12, test_operand13, NULL};

#endif /* CALC_TEST_OPERAND_H */