/*
 *  Data for delete test
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#ifndef CALC_TEST_DELETE_H
#define CALC_TEST_DELETE_H
#include "calc.h"

struct simulation_step test_delete0[] = {
    {
        .state = 
        {
            .memory = 0l,
            .display = 12l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_D
    }, {
        .state = {
            .memory = 0l,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_D
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_D
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    },
};

struct simulation_step test_delete1[] = {
    {
        .state = 
        {
            .memory = 0l,
            .display = 1234l * FP_MULTIPLAYER/100,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 2,
            .digits = 5
        },
        .b = BUTTON_D
    }, {
        .state = 
        {
            .memory = 0l,
            .display = 123l * FP_MULTIPLAYER/10,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 1,
            .digits = 4
        },
        .b = BUTTON_D
    }, {
        .state = 
        {
            .memory = 0l,
            .display = 12l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 0,
            .digits = 3
        },
        .b = BUTTON_D
    }, {
        .state = 
        {
            .memory = 0l,
            .display = 12l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 3
        },
        .b = BUTTON_D
    }, {
        .state = 
        {
            .memory = 0l,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_D
    }, {
        .state = 
        {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_D
    }, {
        .state = 
        {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_D
    }, {
        .state = 
        {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_delete2[] = {
    {
        .state = 
        {
            .memory = 0l,
            .display = 1234l * FP_MULTIPLAYER/100,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 2,
            .digits = 5
        },
        .b = BUTTON_C
    }, {
        .state = 
        {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    },
};

struct simulation_step test_delete3[] = {
    {
        .state = 
        {
            .memory = 0l,
            .display = 1234l * FP_MULTIPLAYER/100,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_OVER,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 2,
            .digits = 5
        },
        .b = BUTTON_D
    }, {
        .state = 
        {
            .memory = 0l,
            .display = 1234l * FP_MULTIPLAYER/100,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_OVER,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 2,
            .digits = 5
        },
        .b = BUTTON_C
    }, {
        .state = 
        {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }, 
};

struct simulation_step *test_delete_s[] =
    {test_delete0, test_delete1, test_delete2, test_delete3, NULL};

#endif /* CALC_TEST_OPERATIONS_H */