/*
 *  Calc - user interface
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#ifndef UI_H
#define UI_H

#include <stdint.h>

#define LED_N 6

#define MINUS 10
#define BLANK 11
#define E 12

extern volatile enum button button_value;
extern volatile uint32_t button_click;

void set_digits(uint8_t d[], uint8_t dp);
void buttons_init();
void ui(void);

#endif /* UI_H */