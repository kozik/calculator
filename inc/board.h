/*
 *  Board
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#ifndef BOARD_H
#define BOARD_H

#include "stm32f030x8.h"

#define LED_PORT GPIOA
#define LED_A (1<<0)
#define LED_F (1<<1)
#define LED_E (1<<2)
#define LED_D (1<<3)
#define LED_DP (1<<4)
#define LED_C (1<<5)
#define LED_G (1<<6)
#define LED_B (1<<7)

#define LED_A_PORT GPIOB
#define LED_A1 (1<<4)
#define LED_A2 (1<<5)
#define LED_A3 (1<<6)
#define LED_A4 (1<<7)
#define LED_A5 (1<<8)
#define LED_A6 (1<<9)

#define K_PORT GPIOB
#define K_1_1 (1<<15)
#define K_1_2 (1<<14)
#define K_1_3 (1<<13)
#define K_1_4 (1<<12)
#define K_2_1 (1<<2)
#define K_2_2 (1<<1)
#define K_2_3 (1<<0)
#define K_2_4 (1<<11)
#define K_2_5 (1<<10)

#endif /* BOARD_H */
