/*
 *  Data for operations test
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#ifndef CALC_TEST_OPERATIONS_H
#define CALC_TEST_OPERATIONS_H
#include "calc.h"

struct simulation_step test_operations0[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 0,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_ADD
    }, {
        .state = {
            .memory = 2l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_ADD,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 2l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_ADD,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_EQ
    }, {
        .state = {
            .memory = 4l * FP_MULTIPLAYER,
            .display = 4l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    },
};

struct simulation_step test_operations1[] = {
    {
		.state = {
			.memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_SIGN,
            .digits = 0
        },
        .b = BUTTON_8,
    }, {
		.state = {
			.memory = 0,
            .display = 8l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_0,
    }, {
		.state = {
			.memory = 0,
            .display = 80l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_DOT,
    }, {
		.state = {
			.memory = 0,
            .display = 80l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 0,
            .digits = 2
        },
        .b = BUTTON_2,
    }, {
		.state = {
			.memory = 0,
            .display = 802l * FP_MULTIPLAYER / 10,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 1,
            .digits = 3
        },
        .b = BUTTON_DIV,
    }, {
		.state = {
			.memory = 802l * FP_MULTIPLAYER / 10,
            .display = 802l * FP_MULTIPLAYER / 10,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_DIV,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_SUB,
    }, {
		.state = {
			.memory = 802l * FP_MULTIPLAYER / 10,
            .display = 0,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_DIV,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_4,
    }, {
		.state = {
			.memory = 802l * FP_MULTIPLAYER / 10,
            .display = 4 * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_DIV,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_ADD,
    }, {
		.state = {
			.memory = -2005l * FP_MULTIPLAYER / 100,
            .display = 2005l * FP_MULTIPLAYER / 100,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_ADD,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_1,
    }, {
		.state = {
			.memory = -2005l * FP_MULTIPLAYER / 100,
            .display = 1l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_ADD,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_EQ
    }, {
		.state = {
			.memory = -1905l * FP_MULTIPLAYER / 100,
            .display = 1905l * FP_MULTIPLAYER / 100,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    }
};

struct simulation_step test_operations2[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 0l,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_5
    }, {
        .state = {
            .memory = 0l,
            .display = 25l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 2
        },
        .b = BUTTON_DIV
    }, {
        .state = {
            .memory = 25l * FP_MULTIPLAYER,
            .display = 25l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_DIV,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 25l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_DIV,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_EQ
    }, {
        .state = {
            .memory = 125l * FP_MULTIPLAYER/10,
            .display = 125l * FP_MULTIPLAYER/10,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_SUB
    }, {
        .state = {
            .memory = 125l * FP_MULTIPLAYER/10,
            .display = 125l * FP_MULTIPLAYER/10,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_SUB,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 125l * FP_MULTIPLAYER/10,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_SUB,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_EQ
    }, {
        .state = {
            .memory = 105l * FP_MULTIPLAYER/10,
            .display = 105l * FP_MULTIPLAYER/10,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    },
};

struct simulation_step test_operations3[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_SUB
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_DOT
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = 0,
            .digits = 2
        },
        .b = BUTTON_D
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_D
    }, {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    },
};

struct simulation_step test_operations4[] = {
    {
        .state = {
            .memory = 0l,
            .display = 0l,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 0l,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_ADD
    }, {
        .state = {
            .memory = 2l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_ADD,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_2
    }, {
        .state = {
            .memory = 2l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_ADD,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 1
        },
        .b = BUTTON_SQRT
    }, {
        .state = {
            .memory = 2l * FP_MULTIPLAYER,
            .display = 2l * FP_MULTIPLAYER,
            .state = CALC_STATE_NEW,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_EQ,
            .sign = FP_NO_SIGN,
            .dot_position = FP_NO_DOT,
            .digits = 0
        },
        .b = BUTTON_NONE
    },
};

struct simulation_step *test_operations[] =
    {test_operations0, test_operations1, test_operations2, test_operations3,
     test_operations4, NULL};

#endif /* CALC_TEST_OPERATIONS_H */