/*
 *  Data for to digit test
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#ifndef CALC_TEST_TO_DIGIT_H
#define CALC_TEST_TO_DIGIT_H
#include "calc.h"

struct ctd {
	struct calc c;
	uint8_t out[FP_DIGITS];
	uint8_t dp;
};

struct ctd ctd_data[] =
{
		{
		.c =
		{
			.display = 1l*FP_MULTIPLAYER,
			.digits = 1,
			.dot_position = FP_NO_DOT,
			.sign = FP_NO_SIGN
		},
		.out = {BLANK, BLANK, BLANK, BLANK, BLANK, 1},
		.dp = FP_NO_DOT,
	}, {
		.c = {
			.display = 0,
			.digits = 0,
			.dot_position = FP_NO_DOT,
			.sign = FP_NO_SIGN
		},
		.out = {BLANK, BLANK, BLANK, BLANK, BLANK, 0},
		.dp = FP_NO_DOT,
	}, {
		.c = {
			.display = 10 * FP_MULTIPLAYER,
			.digits = 3,
			.dot_position = FP_NO_DOT,
			.sign = FP_SIGN
		},
		.out = {BLANK, BLANK, BLANK, MINUS, 1, 0},
		.dp = FP_NO_DOT,
	}, {
		.c =
		{
			.display = FP_PI,
			.digits = FP_PI_DIGIT,
			.dot_position = FP_PI_DOT,
			.sign = FP_NO_SIGN
		},
		.out = {3, 1, 4, 1, 5, 9},
		.dp = FP_PI_DOT,
	}, {
		.c =
		{
			.display = FP_PI,
			.digits = FP_PI_DIGIT,
			.dot_position = FP_NO_DOT,
			.sign = FP_NO_SIGN
		},
		.out = {3, 1, 4, 1, 5, 9},
		.dp = FP_PI_DOT,
	}, {
		.c =
		{
			.display = 1278100,
			.digits = FP_PI_DIGIT,
			.dot_position = FP_NO_DOT,
			.sign = FP_SIGN
		},
		.out = {MINUS, 1, 2, 7, 8, 1},
		.dp = 3,
	}, {
		.c =
		{
			.display = 1278100,
			.digits = FP_PI_DIGIT,
			.dot_position = 3,
			.sign = FP_SIGN
		},
		.out = {MINUS, 1, 2, 7, 8, 1},
		.dp = 3,
	}, {
		.c =
		{
			.display = 11278100,
			.digits = FP_PI_DIGIT,
			.dot_position = FP_NO_DOT,
			.sign = FP_SIGN
		},
		.out = {MINUS, 1, 1, 2, 7, 8},
		.dp = 2,
	}, {
		.c =
		{
			.display = 1111111l * FP_MULTIPLAYER,
			.digits = 0,
			.dot_position = FP_NO_DOT,
			.sign = FP_NO_SIGN
		},
		.out = {BLANK, BLANK, BLANK, BLANK, BLANK, E},
		.dp = FP_NO_DOT
	}, {
		.c =
		{
			.display = 111111l * FP_MULTIPLAYER,
			.digits = 0,
			.dot_position = FP_NO_DOT,
			.sign = FP_SIGN
		},
		.out = {BLANK, BLANK, BLANK, BLANK, BLANK, E},
		.dp = FP_NO_DOT
	}, {
		.c =
		{
			.display = 0l,
			.digits = 6,
			.dot_position = 5,
			.sign = FP_NO_SIGN
		},
		.out = {0, 0, 0, 0, 0, 0},
		.dp = 5
	}, {
		.c =
		{
			.display = 0l,
			.digits = 6,
			.dot_position = 4,
			.sign = FP_SIGN
		},
		.out = {MINUS, 0, 0, 0, 0, 0},
		.dp = 4
	}, {
		.c =
		{
			.display = 111111l * FP_MULTIPLAYER,
			.digits = 0,
			.dot_position = FP_NO_DOT,
			.sign = FP_NO_SIGN
		},
		.out = {1, 1, 1, 1, 1, 1},
		.dp = FP_NO_DOT
	}, {
		.c =
		{
			.display = 111111l * FP_MULTIPLAYER,
			.digits = 0,
			.dot_position = FP_NO_DOT,
			.sign = FP_NO_SIGN,
			.error = CALC_ERROR_DIV_0
		},
		.out = {E, 0, BLANK, BLANK, BLANK, BLANK},
		.dp = FP_NO_DOT
	}, {
		.c =
		{
			.display = 111111l * FP_MULTIPLAYER,
			.digits = 0,
			.dot_position = FP_NO_DOT,
			.sign = FP_NO_SIGN,
			.error = CALC_ERROR_OVER
		},
		.out = {E, 1, BLANK, BLANK, BLANK, BLANK},
		.dp = FP_NO_DOT
	}, {
		.c =
		{
            .memory = 0l,
            .display = 25l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_NONE,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 0,
            .digits = 2
		},
		.out = {BLANK, BLANK, BLANK, BLANK, 2, 5},
		.dp = 0
	}, {
		.c =
		{
            .memory = 0l,
            .display = 25l * FP_MULTIPLAYER,
            .state = CALC_STATE_CONTINUE,
            .error = CALC_ERROR_SQRT_NEG,
            .operand = BUTTON_NONE,
            .sign = FP_NO_SIGN,
            .dot_position = 0,
            .digits = 2
		},
		.out = {E, 2, BLANK, BLANK, BLANK, BLANK},
		.dp = FP_NO_DOT
	},
};

#endif /* CALC_TEST_TO_DIGIT_H */