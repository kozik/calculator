/*
 *  Calc - common definitions
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#ifndef DEFS_H
#define DEFS_H

enum button {
	BUTTON_0 = 0,
	BUTTON_1 = 1,
	BUTTON_2 = 2,
	BUTTON_3 = 3,
	BUTTON_4 = 4,
	BUTTON_5 = 5,
	BUTTON_6 = 6,
	BUTTON_7 = 7,
	BUTTON_8 = 8,
	BUTTON_9 = 9,
	BUTTON_PI = 10,
	BUTTON_DOT,
	BUTTON_SUB,
	BUTTON_ADD,
	BUTTON_MUL,
	BUTTON_DIV,
	BUTTON_SQRT,
	BUTTON_EQ,
	BUTTON_C,
	BUTTON_D,
	BUTTON_NONE
};

#endif /* DEFS_H */