/*
 *  Calc - main state machine
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

 #ifndef CALC_H
 #define CALC_H

#include <inttypes.h>
#include "defs.h"

#ifdef TESTS
#include "../tests/tinytest.h"
#endif /* TESTS */

enum calc_state {
	CALC_STATE_NEW,
	CALC_STATE_CONTINUE
};

enum calc_error {
	CALC_ERROR_NONE,
	CALC_ERROR_DIV_0,
	CALC_ERROR_SQRT_NEG,
	CALC_ERROR_OVER,
	CALC_ERROR
};

enum fp_sign {
	FP_NO_SIGN,
	FP_SIGN
};

struct calc {
	int64_t memory;
	int64_t display;
	enum calc_state state;
	enum calc_error error;
	enum fp_sign sign;
	enum button operand;
	int8_t dot_position;
	int8_t digits;
};

void calc_init(struct calc *c);
void calc_step(struct calc *c, enum button b);
void calc_to_digit(struct calc *c, uint8_t data[], uint8_t *dp);

#ifdef TESTS
struct simulation_step {
	struct calc state;
	enum button b;
};

TINYTEST_DECLARE_SUITE(Calc);
#endif /* TESTS */

#endif /* CALC_H */