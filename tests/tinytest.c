/*
 *  Calc - tests
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#ifndef TINYTEST_NOTESTING

#include "tinytest.h"
#include "../inc/calc.h"

TINYTEST_START_MAIN();
	TINYTEST_RUN_SUITE(Calc);
TINYTEST_END_MAIN();

#endif /* TINYTEST_NOTESTING */