/*
 *  Calc - main state machine
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#ifdef TESTS
#include <string.h>
#endif /* TESTS */

#include "calc.h"
#include "ui.h"

#define FP_DOT			5
#define FP_NO_DOT		-1
#define FP_DIGITS		6

#define FP_PI			314159
#define FP_PI_DOT		5
#define FP_PI_DIGIT		6

#define FP_OVER			100000000000l
#define FP_UNDER		10000000000l

#define FP_MULTIPLAYER		100000

uint32_t multiplayers[] = {100000, 10000, 1000, 100, 10, 1};

static inline void clear(struct calc *c);
static inline void delete(struct calc *c);
static inline void clear_all(struct calc *c);
static inline void error(struct calc *c);

static inline void input_zero(struct calc *c);
static inline void input_number(struct calc *c, enum button b);
static inline void input_dot(struct calc *c);
static inline void input_pi(struct calc *c);
static inline void input_sub(struct calc *c);

static inline void operator_sqrt(struct calc *c);
static inline void operator(struct calc *c, enum button b);

void
calc_init(struct calc *c)
{

	clear_all(c);
}

void
calc_step(struct calc *c, enum button b)
{

	if (c->error != CALC_ERROR_NONE && b != BUTTON_C) {
		return;
	}

	if (b == BUTTON_0) {
		input_zero(c);
	} else if (b <= BUTTON_9) {
		input_number(c, b);
	} else if (b == BUTTON_DOT) {
		input_dot(c);
	} else if (b == BUTTON_PI) {
		input_pi(c);
	} else if (b == BUTTON_SUB) {
		input_sub(c);
	} else if (b == BUTTON_SQRT) {
		operator_sqrt(c);
	} else if (b <= BUTTON_EQ) {
		operator(c, b);
	} else if (b == BUTTON_C) {
		clear_all(c);
	} else if (b == BUTTON_D) {
		delete(c);
	}
}

void
calc_to_digit(struct calc *c, uint8_t data[], uint8_t *dp)
{
	uint8_t data_integral[FP_DIGITS];
	int64_t d_intgral, d_fractional;
	uint8_t p = FP_DIGITS;
	int8_t i, f_len, skip_zero;

	if (c->error != CALC_ERROR_NONE) {
		*dp = FP_NO_DOT;
		for (i = 0; i < FP_DIGITS; i++) {
			data[i] = BLANK;
		}
		data[0] = E;
		switch (c->error) {
			case CALC_ERROR_DIV_0:
				data[1] = 0;
				break;
			case CALC_ERROR_OVER:
				data[1] = 1;
				break;
			case CALC_ERROR_SQRT_NEG:
				data[1] = 2;
				break;
			default:
				break;
		}
		return;
	}

	if (c->sign == FP_SIGN) {
		p--;
	}

	d_intgral = c->display / FP_MULTIPLAYER;
	i = 0;
	do {
		data_integral[i] = d_intgral % 10;
		d_intgral /= 10;
		i++;
		p--;
	} while (d_intgral > 0 && p > 0);

	if (p == 0 && d_intgral != 0) {
		/* overflow */
		data[FP_DIGITS-1] = E;
		for (i = 0; i < FP_DIGITS-1; i++) {
			data[i] = BLANK;
		}
		*dp = FP_NO_DOT;
		return;
	}

	d_fractional = c->display % FP_MULTIPLAYER;
	if (c->dot_position == FP_NO_DOT) {
		f_len = p;
		skip_zero = 1;
	} else {
		f_len = c->dot_position;
		skip_zero = 0;
	}
	d_fractional /= multiplayers[f_len];

	i = FP_DIGITS - 1;
	for (int j = 0; j < f_len; j++) {
		data[i] = d_fractional % 10;
		d_fractional /= 10;
		if (skip_zero != 1 || data[i] != 0) {
			skip_zero = 0;
			i--;
		}
	}

	if (c->dot_position == FP_NO_DOT) {
		*dp = FP_DIGITS - i - 1;
		if (*dp == 0) {
			*dp = FP_NO_DOT;
		}
	} else {
		*dp = c->dot_position;
	}

	for (int j = 0; j < FP_DIGITS - p; j++) {
		data[i] = data_integral[j];
		i--;
	}

	if (c->sign == FP_SIGN) {
		data[i+1] = MINUS;
	}

	for (; i >= 0; i--) {
		data[i] = BLANK;
	}
}

static inline void
clear(struct calc *c)
{

	if (c->state == CALC_STATE_NEW) {
		c->state = CALC_STATE_CONTINUE;
		c->display = 0;
		c->digits = 0;
		c->dot_position = FP_NO_DOT;
		c->sign = FP_NO_SIGN;
	}
}

static inline void
clear_all(struct calc *c)
{

	c->state = CALC_STATE_NEW;
	c->display = 0;
	c->digits = 0;
	c->dot_position = FP_NO_DOT;
	c->sign = FP_NO_SIGN;
	c->operand = BUTTON_NONE;
	c->memory = 0;
	c->error = CALC_ERROR_NONE;
}

static inline void
error(struct calc *c)
{

	c->state = CALC_STATE_NEW;
	c->display = 0;
	c->digits = 0;
	c->dot_position = FP_NO_DOT;
	c->sign = FP_NO_SIGN;
	c->operand = BUTTON_NONE;
	c->memory = 0;
}

static inline void
input_zero(struct calc *c)
{

	if (c->operand == BUTTON_EQ) {
		return;
	}

	if (c->state == CALC_STATE_NEW) {
		clear(c);
		c->state = CALC_STATE_NEW;
	}

	if (c->digits >= FP_DIGITS) {
		return;
	}

	if (c->dot_position == FP_NO_DOT) {
		if (c->display 	!= 0) {
			c->display *= 10;
			c->digits++;
		}
	} else {
		c->digits++;
		c->dot_position++;
	}
}

static inline void
input_number(struct calc *c, enum button b)
{

	if (c->operand == BUTTON_EQ) {
		return;
	}

	clear(c);

	if (c->digits >= FP_DIGITS) {
		return;
	}

	c->digits++;
	if (c->dot_position == FP_NO_DOT) {
		c->display *= 10;
		c->display += b * FP_MULTIPLAYER;
	} else {
		c->dot_position++;
		c->display += b * multiplayers[c->dot_position];
	}
}

static inline void
input_dot(struct calc *c)
{

	if (c->operand == BUTTON_EQ) {
		return;
	}

	clear(c);

	if (c->dot_position == FP_NO_DOT) {
		c->dot_position = 0;
		if (c->display == 0) {
			c->digits++;
		}
	}
}

static inline void
input_pi(struct calc *c)
{

	if (c->operand == BUTTON_EQ) {
		return;
	}

	if (c->state == CALC_STATE_NEW) {
		clear(c);
		c->display = FP_PI;
		c->digits = FP_PI_DIGIT;
		c->dot_position = FP_PI_DOT;
	}
}

static inline void
input_sub(struct calc *c)
{

	if (c->state == CALC_STATE_NEW && c->operand != BUTTON_EQ) {
		clear(c);
		c->sign = FP_SIGN;
		c->digits = 1;
	} else {
		operator(c, BUTTON_SUB);
	}
}

static inline void
operator_sqrt(struct calc *c)
{
	uint64_t x;
	uint64_t tmp, tmp_2;
	uint64_t n = 0;

	operator(c, BUTTON_EQ);

	if (c->display != 0 && c->sign == FP_SIGN) {
		c->error = CALC_ERROR_SQRT_NEG;
		error(c);
		return;
	}

	x = c->display * FP_MULTIPLAYER;

	for (int8_t k = 26; k >= 0; k--) {
		tmp = n + (1l << k);
		tmp_2 = tmp*tmp;
		if (tmp_2 <= x) {
			n = tmp;
			if (tmp_2 == x) {
				break;
			}
		}
	}

	c->display = n;
	c->memory = n;
	c->digits = 0;
	c->dot_position = FP_NO_DOT;
	c->operand = BUTTON_EQ;
	c->state = CALC_STATE_NEW;
	c->sign = FP_NO_SIGN;
}

static inline void
operator(struct calc *c, enum button b)
{
	int64_t signed_in;

	signed_in = c->display;
	if (c->sign == FP_SIGN)
		signed_in *= -1;

	switch (c->operand) {
		case BUTTON_ADD:
			c->memory += signed_in;
			break;
		case BUTTON_SUB:
			c->memory -= signed_in;
			break;
		case BUTTON_MUL:
			c->memory *= signed_in;
			c->memory /= FP_MULTIPLAYER;
			break;
		case BUTTON_DIV:
			if (signed_in == 0) {
				c->error = CALC_ERROR_DIV_0;
				error(c);
				return;
			}
			c->memory *= FP_MULTIPLAYER;
			c->memory /= signed_in;
			break;
		case BUTTON_NONE:
			c->memory = signed_in;
			break;
		default:
			break;
	}

	c->operand = b;
	
	c->display = c->memory;
	if (c->display < 0) {
		c->display *= -1;
		c->sign = FP_SIGN;
		if (c->display >= FP_UNDER) {
			c->error = CALC_ERROR_OVER;
			error(c);
		}
	} else {
		c->sign = FP_NO_SIGN;
		if (c->display >= FP_OVER) {
			c->error = CALC_ERROR_OVER;
			error(c);
		}
	}
	c->state = CALC_STATE_NEW;
	c->dot_position = FP_NO_DOT;
	c->digits = 0;
}

static inline void
delete(struct calc *c)
{
	
	if (c->state == CALC_STATE_NEW) {
		return;
	}

	if (c->dot_position == 0) {
		c->dot_position = FP_NO_DOT;
		if (c->display == 0l) {
			c->digits--;
		}
		if (c->digits == 0) {
			c->state = CALC_STATE_NEW;
		}
		return;
	}

	if (c->digits == 1) {
		c->sign = FP_NO_SIGN;
		c->digits = 0;
		c->display = 0l;
		c->state = CALC_STATE_NEW;
	} else {
		c->digits--;
		if (c->dot_position == FP_NO_DOT) {
			c->display /= 10*FP_MULTIPLAYER;
			c->display *= FP_MULTIPLAYER;			
		} else {
			c->dot_position--;
			c->display /= multiplayers[c->dot_position];
			c->display *= multiplayers[c->dot_position];
		}
	}
}

/**********************************************************
 * TESTS
 *********************************************************/
#ifdef TESTS

static void
print_calc(struct calc *c)
{

	printf("memory:\t\t%ld\n"
	       "display:\t%ld\n"
	       "state:\t\t%d\n"
	       "error:\t\t%d\n"
	       "sign:\t\t%d\n"
	       "dot_pos:\t%d\n"
	       "digits:\t\t%d\n"
		   "operand:\t%d\n",
	       c->memory, c->display, c->state, c->error, c->sign,
	       c->dot_position, c->digits, c->operand);
}

static int
cmp_calc(struct calc *c1, struct calc *c2)
{

	if (c1->memory != c2->memory)
		return -1;
	if (c1->display != c2->display)
		return -1;
	if (c1->state != c2->state)
		return -1;
	if (c1->error != c2->error)
		return -1;
	if (c1->sign != c2->sign)
		return -1;
	if (c1->dot_position != c2->dot_position)
		return -1;
	if (c1->digits != c2->digits)
		return -1;
	if (c1->operand != c2->operand)
		return -1;

	return 0;
}

static int
simulate(struct simulation_step **simulations)
{
	struct calc c;
	int i = 0;
	int j = 0;

	while (simulations[i] != NULL) {
		j = 0;
		memcpy(&c, &simulations[i][0].state, sizeof(struct calc));
		do {
			calc_step(&c, simulations[i][j].b);
			if (cmp_calc(&c, &simulations[i][j+1].state) != 0) {
				printf("\nexpected[%d][%d]:\n", i, j+1);
				print_calc(&simulations[i][j+1].state);
				printf("\nactual:\n");
				print_calc(&c);
				return -1;
			}
			j++;
		} while (simulations[i][j].b != BUTTON_NONE);
		i++;
	}

	return 1;
}

#include "calc_test_input.h"
static int
test_input(const char *name)
{

	TINYTEST_EQUAL_MSG(simulate(test_input_s), 1, "calc_test_input\n")
	return 1;
}

#include "calc_test_delete.h"
static int
test_delete(const char *name)
{

	TINYTEST_EQUAL_MSG(simulate(test_delete_s), 1, "calc_test_delete.\n")
	return 1;
}


#include "calc_test_operand.h"
static int
test_operand(const char *name)
{

	TINYTEST_EQUAL_MSG(simulate(test_operand_s), 1, "calc_test_operand\n")
	return 1;
}

#include "calc_test_operations.h"
static int
test_simple_operations(const char *name)
{

	TINYTEST_EQUAL_MSG(simulate(test_operations), 1,
					   "test_simple_operations\n")
	return 1;
}

#include "calc_test_to_digit.h"
static int
test_calc_to_digit(const char *name)
{
	uint8_t data[FP_DIGITS];
	uint8_t dp;

	for (int i = 0; i < sizeof(ctd_data)/sizeof(struct ctd); i++) {
		calc_to_digit(&ctd_data[i].c, data, &dp);
		for (int j = 0; j < FP_DIGITS; j++) {
			if (ctd_data[i].out[j] != data[j]) {
				printf("should be: %d is: %d at i=%d, j=%d\n",
					   ctd_data[i].out[j], data[j], i, j);
				TINYTEST_EQUAL_MSG(0, 1, "test_calc_to_digit\n");
			}
		}
		if (ctd_data[i].dp != dp) {
			printf("should be: %d is: %d at i=%d\n",
				   ctd_data[i].dp, dp, i);
			TINYTEST_EQUAL_MSG(0, 1, "test_calc_to_digit\n");
		}
	}
	return 1;
}

TINYTEST_START_SUITE(Calc);
	TINYTEST_ADD_TEST(test_input, NULL, NULL);
	TINYTEST_ADD_TEST(test_delete, NULL, NULL);
	TINYTEST_ADD_TEST(test_operand, NULL, NULL);
	TINYTEST_ADD_TEST(test_simple_operations, NULL, NULL);
	TINYTEST_ADD_TEST(test_calc_to_digit, NULL, NULL);
TINYTEST_END_SUITE();

#endif /* TESTS */