/*
 *  Calc
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#include <stdint.h>
#include "stm32f030x8.h"

#include "board.h"
#include "defs.h"
#include "ui.h"
#include "calc.h"

int
main(void)
{
	struct calc c;
	uint8_t data[LED_N];
	enum button bv;
	uint8_t dp;

	RCC->AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM14EN;

	/* 8MHz / 16 / 1000 = 500Hz */
	TIM14->CR1 |= TIM_CR1_CEN;
	TIM14->DIER |= TIM_DIER_UIE;
	TIM14->PSC = 16 - 1;
	TIM14->ARR = 1000;
	NVIC->ISER[0] |= 1<<19;

	LED_PORT->MODER |= GPIO_MODER_MODER0_0 | GPIO_MODER_MODER1_0 |
			GPIO_MODER_MODER2_0 | GPIO_MODER_MODER3_0 |
			GPIO_MODER_MODER4_0 | GPIO_MODER_MODER5_0 |
			GPIO_MODER_MODER6_0 | GPIO_MODER_MODER7_0;
	LED_A_PORT->MODER |= GPIO_MODER_MODER4_0 | GPIO_MODER_MODER5_0 |
			GPIO_MODER_MODER6_0 | GPIO_MODER_MODER7_0 |
			GPIO_MODER_MODER8_0 | GPIO_MODER_MODER9_0;
	/* button */
	K_PORT->OTYPER |= GPIO_OTYPER_OT_12 | GPIO_OTYPER_OT_13 |
			GPIO_OTYPER_OT_14 | GPIO_OTYPER_OT_15;
	K_PORT->MODER |= GPIO_MODER_MODER12_0 | GPIO_MODER_MODER13_0 |
			GPIO_MODER_MODER14_0 | GPIO_MODER_MODER15_0;
	K_PORT->PUPDR |= GPIO_PUPDR_PUPDR0_0 | GPIO_PUPDR_PUPDR1_0 |
					 GPIO_PUPDR_PUPDR2_0 | GPIO_PUPDR_PUPDR10_0 |
					 GPIO_PUPDR_PUPDR11_0;
	K_PORT->BSRR = K_1_1 | K_1_2 | K_1_3 | K_1_4;

	buttons_init();
	calc_init(&c);
	__enable_irq();

	while (1)
	{
		if (button_click != 0) {
			__disable_irq();
			bv = button_value;
			button_click = 0;
			calc_step(&c, bv);
			calc_to_digit(&c, data, &dp);
			set_digits(data, LED_N - dp - 1);
			__enable_irq();
		}
	}
}