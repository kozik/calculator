/*
 *  Calc - user interface
 *  Copyright 2018, Rafal Kozik
 *  Licence: Apache 2.0
 */

#include "ui.h"
#include "board.h"
#include "defs.h"

#define B_N_1 4
#define B_N_2 5

#define DEBONUCE_ITERATIONS 5

enum button_states {
	BUTTON_STATE_RELEASE,
	BUTTON_STATE_DEB_DETECT,
	BUTTON_STATE_DETECT,
	BUTTON_STATE_DEB_RELEASE,
};

struct button_state {
	enum button_states state;
	uint32_t counter;
	enum button value;
};

static const uint32_t NUM[] = {0xAF, 0xA0, 0xCD, 0xE9, 0xE2, 0x6B, 0x6F, 0xA1,
							   0xEF, 0xEB, 0x40, 0x00, 0x4F};
static const uint32_t LED_A_PIN[] = {LED_A1, LED_A2, LED_A3, LED_A4, LED_A5,
									 LED_A6};
static const uint32_t B_K_1[] = {K_1_1, K_1_2, K_1_3, K_1_4};
static const uint32_t B_K_2[] = {K_2_1, K_2_2, K_2_3, K_2_4, K_2_5};

static volatile uint32_t data[LED_N] = {0, 0, 0, 0, 0, 0xAF};
static struct button_state buttons[B_N_1][B_N_2];
volatile enum button button_value = BUTTON_NONE;
volatile uint32_t button_click = 0;

void
set_digits(uint8_t d[], uint8_t dp)
{

	for (int i = 0; i < LED_N; i++) {
		if (d[i] <= E) {
			data[i] = NUM[d[i]];
		} else {
			data[i] = 0;
		}
		if (dp == i) {
			data[i] |= LED_DP;
		}
	}
}

static void
show_next()
{
	static uint32_t next_led;

	LED_A_PORT->BSRR = LED_A1 | LED_A2 | LED_A3 | LED_A4 | LED_A5 | LED_A6;
	LED_PORT->BRR = LED_A | LED_B | LED_C | LED_D | LED_E | LED_F | LED_G | LED_DP;
	LED_PORT->BSRR = data[next_led];
	LED_A_PORT->BRR = LED_A_PIN[next_led];
	next_led++;
	if (next_led >= LED_N) {
		next_led = 0;
	}
}

void
buttons_init()
{

	buttons[0][0].value = BUTTON_D;
	buttons[0][1].value = BUTTON_7;
	buttons[0][2].value = BUTTON_4;
	buttons[0][3].value = BUTTON_1;
	buttons[0][4].value = BUTTON_DOT;

	buttons[1][0].value = BUTTON_C;
	buttons[1][1].value = BUTTON_8;
	buttons[1][2].value = BUTTON_5;
	buttons[1][3].value = BUTTON_2;
	buttons[1][4].value = BUTTON_0;

	buttons[2][0].value = BUTTON_PI;
	buttons[2][1].value = BUTTON_9;
	buttons[2][2].value = BUTTON_6;
	buttons[2][3].value = BUTTON_3;
	buttons[2][4].value = BUTTON_EQ;

	buttons[3][0].value = BUTTON_SQRT;
	buttons[3][1].value = BUTTON_DIV;
	buttons[3][2].value = BUTTON_MUL;
	buttons[3][3].value = BUTTON_SUB;
	buttons[3][4].value = BUTTON_ADD;
}

static void
button_n_state(struct button_state *b, uint32_t in)
{
	switch (b->state) {
	case BUTTON_STATE_RELEASE:
		if (in == 0) {
			b->counter = DEBONUCE_ITERATIONS;
			b->state = BUTTON_STATE_DEB_DETECT;
		}
		break;
	case BUTTON_STATE_DEB_DETECT:
		if (in == 0) {
			b->counter--;
			if (b->counter == 0) {
				b->state = BUTTON_STATE_DETECT;
				/* clicked */
				button_value = b->value;
				button_click = 1;
			}
		} else {
			b->state = BUTTON_STATE_RELEASE;
		}
		break;
	case BUTTON_STATE_DETECT:
		if (in != 0) {
			b->counter = DEBONUCE_ITERATIONS;
			b->state = BUTTON_STATE_DEB_RELEASE;
		}
		break;
	case BUTTON_STATE_DEB_RELEASE:
		if (in != 0) {
			b->counter--;
			if (b->counter == 0) {
				b->state = BUTTON_STATE_RELEASE;
			}
		} else {
			b->state = BUTTON_STATE_DETECT;
		}
		break;
	default:
		b->state = BUTTON_STATE_RELEASE;
		break;
	}
}

static void
button()
{
	static uint32_t next;
	uint32_t in;

	in = K_PORT->IDR;
	for (uint32_t i = 0; i < B_N_2; i++) {
		button_n_state(&buttons[next][i], in & B_K_2[i]);
	}

	next++;
	if (next >= B_N_1) {
		next = 0;
	}
	K_PORT->BSRR = K_1_1 | K_1_2 | K_1_3 | K_1_4;
	K_PORT->BRR = B_K_1[next];
}

void
ui(void)
{

	TIM14->SR = 0;
	show_next();
	button();
}